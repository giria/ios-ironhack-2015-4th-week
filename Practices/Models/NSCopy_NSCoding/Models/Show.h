//
//  Product.h
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/MTLModel.h>
#import <Mantle/MTLModel+NSCoding.h>

@interface Show : MTLModel
@property (copy,nonatomic) NSString *showId;
@property (copy,nonatomic) NSString *showDescription;
@property (copy,nonatomic) NSString *showTitle;
@property (assign,nonatomic) CGFloat showRating;
@end

//@interface Show(NSCopying)<NSCopying>
//
//@end
//
//@interface Show(NSCoding)<NSCoding>
//
//@end
//
//@interface Show(Equality)
//- (BOOL)isEqualToShow:(Show *)show;
//@end