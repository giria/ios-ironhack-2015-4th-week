#Clean Code + Clean Architecture

## IronHack iOS Bootcamp 
##![100%,inline](assets/ironhacklogo.png)
### Additional Lectures


####Daniel García - Produkt

---

![fit](assets/unclebob.jpg)

---

![fit](assets/unclebob.jpg)
#Robert C. Martin

---

![fit](assets/cleancode.jpg)

---

#S.O.L.I.D.


---

#**S**.O.L.I.D.

## Single Responsibility Principle (SRP)

###"A class should have only a single responsibility (i.e. only one potential change in the software's specification should be able to affect the specification of the class)"

---

#S.**O**.L.I.D.

## Open/Close Principle (OCP)

###“software entities … should be open for extension, but closed for modification.”

---

#S.O.**L**.I.D.

## Liskov Substitution Principle (LSP)

###“objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program.”

---

#S.O.L.**I**.D.

## Interface segregation principle (ISP)

###“many client-specific interfaces are better than one general-purpose interface.”

---

#S.O.L.I.**D**.

## Dependency Inversion principle (DIP)

###“Depend upon Abstractions. Do not depend upon concretions.”


---

![fit](assets/CleanArchitecture.jpg)

---

#What is the architecture of an application ?

---

![fit](assets/theDatabase.jpg)

---

#A good architect defers important decisions

---

![fit](assets/library.jpg)

---

![fit](assets/library.jpg)
#LIBRARY !!

---

![fit](assets/church-floor-plans.com.jpg)

---

![fit](assets/church-floor-plans.com.jpg)
#CHURCH !!

---

#A good architecture must scream its purpose

---

#System Architecture - Delivery Mechanism

## Your application should be independent of how is it used. 

---

#System Architecture 
##Contains all the application business logic. 
###What does your application do ?

---

#Delivery Mechanism
##Defines how your users will interact with your application
###Visible part of your application (iPhone,iPad,OS X,console...)

---

![fit](assets/BoundariesIsolateDMCropped.jpg)

---

#Use Cases

---

![left fit](assets/oosofteng_use_case_approach.jpg)
![right fit](assets/jacobson.jpg)

---

![left fit](assets/oosofteng_use_case_approach.jpg)
![right fit](assets/jacobson.jpg)

#Object-Oriented Software Engineering: A Use Case Driven Approach
##By Ivar Jacobson

---

# Use Cases
###A use case is all the ways of using a system to achieve a particular goal for a particular user

---

##Yeah...

##But how does this Clean Architecture thing works with my loved MVC ?

![right fit](assets/MVC.jpg)

---

![fit](assets/ResponseViewModel.jpg)

---

![fit](assets/ResponseViewModelDimmed.png)
##Part of MVC belongs to the Delivery Mechanism, other part belongs to the Application Main Architecture
