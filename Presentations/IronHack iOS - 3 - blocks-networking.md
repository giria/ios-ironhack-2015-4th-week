#Blocks

## IronHack iOS Bootcamp 
##![100%,inline](assets/ironhacklogo.png)
### Week 4 - Day 3


####Daniel García - Produkt
---

#Blocks

#`^{ }`

---

#Blocks

#`^{ //AWESOME }`

---

#Block's anatomy

```objectivec
[UIView animateWithDuration:0.3 animations:^{
        self.alpha=0.0;
} completion:^(BOOL finished) {
        
}];
```
---

#Block's anatomy

`^` -> Control character. Starts a block syntax
`(BOOL finished)` -> Block's receiving parameter
`{}`-> Content of a block


---

#Block's anatomy

`^` -> Control character. Starts a block syntax
`(BOOL finished)` -> Block's receiving parameter
`{}`-> Content of a block

#http://fuckingblocksyntax.com


---

# Blocks Syntax

###Local variable
```objectivec
returnType (^blockName)(parameterTypes) = ^returnType(parameters) {...};
```

###property
```objectivec
@property (nonatomic, copy) returnType (^blockName)(parameterTypes);
```

---

# Blocks Syntax

###method parameter
```objectivec
- (void)someMethodThatTakesABlock:(returnType (^)(parameterTypes))blockName;
```

###argument to a method call
```objectivec
[someObject someMethodThatTakesABlock: ^returnType (parameters) {...}];
```

---

#Blocks Syntax

###typedef
```objectivec
typedef returnType (^TypeName)(parameterTypes);
TypeName blockName = ^returnType(parameters) {...};
```

---

#Memory management using blocks
#MAGIC!

---

#Memory management using blocks
##A block: new scope, new memory stack frame
##Stack Object

---

#Stack vs Heap

---

#Objects referenced inside a block are automatically retained by the block

---

##Objects referenced inside a block are automatically retained by the block

```objectivec
- (void)updateHomeTownForUser:(UserEntity *)user{
	NSDate *requestDate=[NSDate date];
	[self.requestManager homeTownForUser:user completion:^(CityEntity *resultCity){
		user.hometown=resultCity;
		user.lastHometownUpdate=requestDate;
	}];
}
```

###`requestDate` and `user` are retained by the block

---

#Same memory address, different pointer inside the stack

```objectivec
NSDate *requestDate=[NSDate date]; /// 0x000001
[self.requestManager homeTownForUser:user completion:^(CityEntity *resultCity){
	user.lastHometownUpdate=requestDate; /// 0x000005
}];
```
---

#Scalars and structs referenced inside a block are *copied*
###`NSInteger`,`CGFloat`,`CGRect`,...

---

#Block ≈ Object

--- 

#retain a block

---

#retain a block

```objectivec
@interface UserFetcher()
@property (nonatomic, copy) void (^fetchUsersCallback)();
@end
- (void)fetchUsersWithCompletionBlock:(void (^)())completion{
	self.fetchUsersCallback=completion;
}
```

A block behaves as an object, but we never retain it. Instead, we copy it.

--- 

#PRACTICE

---


#retain cycles

```objectivec

- (void)updateUserHomeTown{
	[self.requestManager homeTownForUser:self.user 
							  completion:^(CityEntity *resultCity){
		self.homeTownCity=resultCity;
	}];
} 

```

###`self` is retained by the block, the block is retained by `requestManager`, that is retained by self

--- 

![](assets/strong-ref-cycle-weak-ref.png)

--- 
#__weak
![](assets/strong-ref-cycle-weak-ref.png)

---

# __weak

```objectivec
- (void)updateUserHomeTown{
	__weak typeof(self) weakSelf=self;
	[self.requestManager homeTownForUser:self.user 
							  completion:^(CityEntity *resultCity){
		weakSelf.homeTownCity=resultCity;
	}];
} 
```

As `weakSelf` is a weak reference (doesn't increase the retain count), it breaks the retain cycle.
If `self` is released, weakSelf would be `nil` when we try to reach it

---

##What if we want to ensure the operation executes?

---

##What if we want to ensure the operation executes?

#__strong

---

# __strong

```objectivec
- (void)updateUserHomeTown{
	__weak typeof(self) weakSelf=self;
	[self.requestManager homeTownForUser:self.user 
							  completion:^(CityEntity *resultCity){
		__strong typeof(weakSelf) strongSelf = weakSelf;
		strongSelf.homeTownCity=resultCity;
	}];
} 
```

---
# __strong

```objectivec
- (void)updateUserHomeTown{
	__weak typeof(self) weakSelf=self;
	[self.requestManager homeTownForUser:self.user 
							  completion:^(CityEntity *resultCity){
		__strong typeof(weakSelf) self = weakSelf;
		self.homeTownCity=resultCity;
	}];
} 
```
---

#PRACTICE

---

#@weakify() & @strongify()

##libextobjc

###https://github.com/jspahrsummers/libextobjc

---
# Without @weakify @strongify

```objectivec
- (void)updateUserHomeTown{
	__weak typeof(self) weakSelf=self;
	[self.requestManager homeTownForUser:self.user 
							  completion:^(CityEntity *resultCity){
		__strong self typeof(weakSelf)=weakSelf;
		self.homeTownCity=resultCity;
	}];
} 
```
---
# With @weakify @strongify

```objectivec
- (void)updateUserHomeTown{
	@weakify(self);
	[self.requestManager homeTownForUser:self.user 
							  completion:^(CityEntity *resultCity){
		@strongify(self);
		self.homeTownCity=resultCity;
	}];
} 
```
---

# __block

```objectivec
- (UserEntity*)loggedUserInUsersArray:(NSArray *)allUsers{
	__block UserEntity *loggedUser;
	[allUsers enumerateObjectsUsingBlock:^(UserEntity *user, 
											NSUInteger idx, 
											BOOL *stop) {
		if (user.loggedUser){
			loggedUser=user;
		}      
	}];
	return loggedUser;
}
```

---

# __assign

```objectivec
- (NSUInteger)numberOfMaleUsers:(NSArray *)allUsers{
	__assign NSUInteger numberOfMaleUsers=0;
	[allUsers enumerateObjectsUsingBlock:^(UserEntity *user, 
											NSUInteger idx, 
											BOOL *stop) {
		if (user.gender==GenderMale){
			numberOfMaleUsers++;
		}      
	}];
	return numberOfMaleUsers;
}
```

---

#PRACTICE

--- 

![](assets/afnetworking-logo.png)

--- 

#AFNetworking

##Most used network access library in objective-c (iOS/OSX compatible)

###https://github.com/AFNetworking/AFNetworking
###@mattt

--- 

#AFNetworking
