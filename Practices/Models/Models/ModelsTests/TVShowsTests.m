//
//  TVShowsTests.m
//  Models
//
//  Created by Daniel García on 27/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Show.h"

@interface TVShowsTests : XCTestCase

@end

@implementation TVShowsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testJSONParse {
    NSError *error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"shows" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    for (NSDictionary *showData in [JSONDictionary valueForKey:@"shows"]) {
        NSError *error;
        Show *newShow = [MTLJSONAdapter modelOfClass:[Show class] fromJSONDictionary:showData error:&error];
    }
    
    
}

@end
